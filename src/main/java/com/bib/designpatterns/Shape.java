package com.bib.designpatterns;

public interface Shape {
    void draw();
}
