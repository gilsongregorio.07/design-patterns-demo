package com.bib.designpatterns;

public class BuilderPatternDemo {
    public static void main(String[] args) {
        User user1 = new User.UserBuilder("Mark Gilson", "Gregorio")
                .age(22)
                .phone("09665823615")
                .address("Fake address 1234")
                .build();

        System.out.println(user1);
    }
}
